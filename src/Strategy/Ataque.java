/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Strategy;

import dungeons_and_dragons.core.Caballero;
import dungeons_and_dragons.core.Personaje;
import dungeons_and_dragons.core.Rey;
import dungeons_and_dragons.core.Troll;
import interfaz.iAtaque;

/**
 *
 * @author cisco
 */
public class Ataque {

    //atributos
    int dano;
    String nombreAtaque;

    //constructores
    public Ataque(String pName, int pDano) {
        this.nombreAtaque = pName;
        this.dano = pDano;
    }
    //gettters & setters

    //metodos
    @Override
    public String toString() {
        return "Ataque [" + "=>nombreAtaque=" + nombreAtaque + " => dano=" + dano + ']';
    }

    public void messageAtaque(Personaje pEnemigo) {
        //condicionales para hacer mas legible el else if

        // siEsTroll => miramos que el parametro sea una instancia de un personaje
        // determinado y ademas nos aseguramos de que los puntos de vida sean los indicados
        boolean siEsTroll = pEnemigo instanceof Troll && (pEnemigo.vida == 1000 || pEnemigo.vida == 1250 || pEnemigo.vida == 1500);
        boolean siEsRey = pEnemigo instanceof Rey && pEnemigo.vida == 2000;
        boolean siEsCaballero = pEnemigo instanceof Caballero && pEnemigo.vida == 1500;
        // boolean siRegeneraVida = this.nombreAtaque=="Regenera vida";

        if (pEnemigo.vida <= 0) {
            System.out.println("\n" + pEnemigo.nombre + " ha muerto\n");
        } else if (siEsTroll || siEsRey || siEsCaballero) {
            System.out.println(" ataque con " + this.nombreAtaque + " => " + pEnemigo.nombre + ": " + (pEnemigo.vida -= this.dano) + "\n");
        }/*else if (siRegeneraVida) {            
            System.out.println(" utiliza hechizo " + this.nombreAtaque + " => " +  this.dano + "\n");
        }*/ else {
            System.out.println(" ataque con " + this.nombreAtaque + " => " + pEnemigo.nombre + ": " + pEnemigo.vida + "\n");
        }
    }

    /**
     * Metodo que se implementa desde la interfaz iAtaque 
     * Muestra un mensaje que se declara en la funcion messageAtaque();
     * y retorna el la vida que le queda al personaje luego de restar el daño
     *
     * @param pEnemigo
     * @return personajeRival.vida
     */
    public int lanzaAtaque(Personaje pEnemigo) {
        messageAtaque(pEnemigo);
        return pEnemigo.vida -= this.dano;
    }

}
