package dungeons_and_dragons.methods;

import Strategy.AtaqueArco;
import Strategy.AtaqueCuchillo;
import Strategy.AtaqueEspada;
import Strategy.RegeneraVida;
import Strategy.Ataque;
import dungeons_and_dragons.core.Caballero;
import dungeons_and_dragons.core.Personaje;
import static dungeons_and_dragons.core.Personaje.ejercitoHombres;
import static dungeons_and_dragons.core.Personaje.ejercitoTrolls;
import dungeons_and_dragons.core.Rey;
import dungeons_and_dragons.core.Troll;
import java.util.Random;
import java.util.Scanner;
import interfaz.iAtaque;
import java.util.ArrayList;

/**
 *
 * @author cisco
 */
public class Methods {

    Scanner s = new Scanner(System.in);
    // defino variables globales para llamarlas en funciones
    Random random = new Random();
    int contador = 0; //contador que se usara para la info de las rondas en la batalla
    Ataque defineAtaque;
    Rey rey;
    Caballero cab;
    Troll troll;
    AtaqueArco arco = new AtaqueArco("Arco", 50);
    AtaqueEspada espada = new AtaqueEspada("Espada", 100);
    AtaqueCuchillo cuchillo = new AtaqueCuchillo("Cuchillo", 25);
    RegeneraVida vida = new RegeneraVida("Regenera vida", -75);
    ArrayList<Ataque> listaDeAtaques = new ArrayList();

    /**
     * añade los ataques a un arraylist, genera un numero random en base al
     * tamaño de la lista y devuelve el ataque guardado en esa posicion
     *
     * @return ataque de la listaDeAtaques
     */
    public Ataque addAtaqueRandomFromList() {
        listaDeAtaques.add(espada);
        listaDeAtaques.add(arco);
        listaDeAtaques.add(cuchillo);
        //listaDeAtaques.add(vida);

        int index = random.nextInt(listaDeAtaques.size());

        return listaDeAtaques.get(index);
    }

    /*
     ********************
     *       MENU       * 
     ********************
     */
    /**
     * Menu para iniciar la aplicacion. TODO : añadir opciones a medida que se
     * implementen new features! 1.- crear ejercito heroes 2.- cerrar app
     */
    public void menuprincipal() {

        int opcion;
        opcion = Integer.parseInt(s.next());
        while (opcion != 2) {
            switch (opcion) {
                case 1:
                    System.out.println("Formar ejercito de Heroes");
                    System.out.println("__________________________");
                    System.out.println("");
                    System.out.println("Cuantos heroes formaran el ejercito");
                    int pCantidadSoldados = s.nextInt();
                    crearEjercitoHeroes(pCantidadSoldados);

                    submenuElijeDificultad();

                    break;
                case 2:
                    break;
                default:
                    System.out.println("algo a ido mal");
                    break;
            }
        }
    }

    /**
     * submenu que aparece despues de crear el ejercito de heroes. crea ejercito
     * rival en funcion al nivel seleccionado, a mayor nivel mas items en el
     * array y mas ptos de vida del item, muestra info de lo que se ha generado
     * e inicia la batalla
     */
    public void submenuElijeDificultad() {
        // Submenu para continuar con los parametros de juego
        int dificultad = 0;
        do {
            System.out.println("Nivel de dificultad");
            System.out.println("___________________");

            System.out.println("Elige un nivel:\n1.- Baja\n2.- Media\n3.- Alta");
            dificultad = Integer.parseInt(s.next());

            switch (dificultad) {
                case 1:
                    crearEjercitoTroll(9, 2, 1000);//minimo = 2, maximo = 10, puntos de vida = 1000
                    infoEjercitoTroll();
                    comenzarBatalla();
                    break;
                case 2:
                    crearEjercitoTroll(16, 5, 1250);//minimo = 5, maximo = 20, puntos de vida = 1250
                    infoEjercitoTroll();
                    comenzarBatalla();
                    break;
                case 3:
                    crearEjercitoTroll(23, 8, 1500);//minimo = 8, maximo = 30, puntos de vida = 1000
                    infoEjercitoTroll();
                    comenzarBatalla();
                    break;
                default:
                    System.out.println("Opcion no válida, elige un nivel válido");
                    submenuElijeDificultad();
                    break;
            }
        } while (dificultad != 3);

    }

    /*
     *******************************
     *       CREAR EJERCITOS       * 
     *******************************
     */
    /**
     * creaa los ejercitos de hombres manualmente. se pasa como parametro
     * canitdad item formaran el ejercito, primer item clase rey con mas ptos de
     * vida, el resto items: caballeros. setearemos el ataque para cada item que
     * se crea, se muestra info del array creado
     *
     * @param pCantidadSoldados
     */
    public void crearEjercitoHeroes(int pCantidadSoldados) {
        Scanner s = new Scanner(System.in);

        String pNombre;
        System.out.println("Introduce nombre del rey");
        pNombre = s.nextLine();
        // creao la instancia del rey y le paso el nombre introducido por teclado
        rey = new Rey(pNombre);
        // llamada al metodo elijeAtaque()
        elijeAtaque(rey);
        // introduzco al rey en el array del ejercito
        ejercitoHombres.add(rey);

        // itero sobre el array en funcion de la cantidad de soldados que tendra el
        // array
        for (int i = 1; i < pCantidadSoldados; i++) {
            System.out.println("introduce nombre del caballero");
            // le doy un nombre al caballero
            pNombre = s.nextLine();
            // creo la instancia de caballero
            cab = new Caballero(pNombre);
            // llamada al metodo elijeAtaque()
            elijeAtaque(cab);
            // añado el caballero creado al array;
            ejercitoHombres.add(cab);
        }
        infoEjercitoHombres();
    }

    /**
     * funcion para crear ejercito de trolls automaticamente. pasamos como
     * parametros el minimo y el maximo de items del array y los puntos de vida
     * con los que inicializan los trolls
     *
     * @param pMin
     * @param pMax
     * @param pVida
     */
    public void crearEjercitoTroll(int pMin, int pMax, int pVida) {
        Random random = new Random();

        int rand = random.nextInt(pMax) + pMin;// pMax para maximo items y pMin para minimo items en array
        for (int i = 1; i <= rand; i++) {
            ejercitoTrolls.add(new Troll("Troll " + i, pVida));// inicializamos Troll con vida 1000
        }
        for (Personaje troll : ejercitoTrolls) {
            troll.setAtaque(addAtaqueRandomFromList());
        }
    }

    /*
     *******************************
     *        INFO EJERCITOS       * 
     *******************************
     */
    /**
     * muestra la info del ejercito (a modo de debug) tamaño y miembros del
     * arraylist
     */
    public void infoEjercitoTroll() {
        System.out.println("El ejercito rival esta compuesto por: " + ejercitoTrolls.size() + " trolls");
        System.out.print("[");
        for (Personaje troll : ejercitoTrolls) {
            System.out.print(troll.toString());
            System.out.print(", ");
        }
        System.out.println("]");

        System.out.println("");
    }

    public void infoEjercitoHombres() {
        System.out.println("Ejercito creado correctamente");
        System.out.println("Tu ejercito esta compuesto por: " + ejercitoHombres.size() + " soldados");
        // muestro los nombres de las instancias/elementos que he añadido al ejercito
        System.out.print("[");
        for (Personaje hombre : ejercitoHombres) {
            System.out.print(hombre.toString());
            System.out.print(", ");
        }
        System.out.println("]");

        System.out.println("");
    }

    /*
     *******************************
     *       SETEAR ATAQUE         * 
     *******************************
     */
    /**
     * en funcion del case seleccionado en la funcion elijeAtaque() recoge los
     * parametros que esta le define, comprueba a que clase pertenece el obj y
     * avanza al metodo setAtaque() del obj
     *
     * @param pPersonaje
     * @param pDefineAtaque
     */
    public void setearAtaque(Personaje pPersonaje, Ataque pDefineAtaque) {
        //Ataque defineAtaque = null ;

        //boolean siEsTroll = pPersonaje instanceof Troll;
        boolean siEsRey = pPersonaje instanceof Rey;
        boolean siEsCaballero = pPersonaje instanceof Caballero;

        if (siEsCaballero) {
            cab.setAtaque((Ataque) pDefineAtaque);
        } else if (siEsRey) {
            rey.setAtaque((Ataque) pDefineAtaque);
        }
    }

    /**
     * muestra un switch en el que seleccionar el tipo de ataque que queremos
     * para nuestro personaje una vez seleccionado el ataque entra en la funcion
     * setearAtaque()
     *
     * @param pPersonaje
     */
    public void elijeAtaque(Personaje pPersonaje) {

        // defino que ataque va a tener el caballero
        System.out.println(
                "Selecciona el ataque entre los siguentes:\n1.-Espada\n2.-Arco\n3.-Cuchillo\n4.-Regenera vida");
        int ataque = Integer.parseInt(s.next());

        switch (ataque) {
            case 1:
                defineAtaque = new AtaqueEspada("Espada", 100);
                setearAtaque(pPersonaje, defineAtaque);
                break;
            case 2:
                defineAtaque = new AtaqueArco("Arco", 50);
                setearAtaque(pPersonaje, defineAtaque);
                break;
            case 3:
                defineAtaque = new AtaqueCuchillo("Cuchillo", 25);
                setearAtaque(pPersonaje, defineAtaque);
                break;
            case 4:
                defineAtaque = new RegeneraVida("Regenera vida", -75);
                setearAtaque(pPersonaje, defineAtaque);
                break;
            default:
                System.out.println("ataque no valido, vuelva a intentarlo");
        }
    }

    /*
     ********************************
     *       COMENZAR BATALLA       * 
     ********************************
     */
    public void ataqueHeroes() {
        for (Personaje hombre : ejercitoHombres) {

            int randomTroll = random.nextInt(ejercitoTrolls.size());

            if (ejercitoTrolls.get(randomTroll).vida <= 0) {
                System.out.println(hombre.nombre + " ataca a " + ejercitoTrolls.get(randomTroll) + "\n" + ejercitoTrolls.get(randomTroll).nombre + " ha muerto");
                System.out.println("Se ha eliminado del ejercito de trolls a " + ejercitoTrolls.get(randomTroll).nombre);
                ejercitoTrolls.remove(ejercitoTrolls.get(randomTroll));
                if (ejercitoTrolls.size() <= 0) {
                    System.out.println("Ya no quedan miembros en el ejercito de Trolls!!\nLos HEROES han conquistado la batalla\nUn nuevo día esta por salir, se vislumbra prosperidad en el horizonte");
                    break;
                } else {
                    System.out.println("_______________________________");
                }
                // mostrar los que quedan en el array a modo de debug
                infoEjercitoTroll();
            } else {
                if (hombre instanceof Rey) {
                    for (int j = 0; j < 3; j++) {
                        // System.out.println("que comience el ataque");
                        System.out.println(hombre.nombre + ", " + hombre.vida + " ataca a " + ejercitoTrolls.get(randomTroll));
                        hombre.ataca(ejercitoTrolls.get(randomTroll));
                    }
                    System.out.println("_______________________________");
                }
                if (hombre instanceof Caballero) {
                    for (int j = 0; j < 2; j++) {
                        System.out.println(hombre.nombre + ", " + hombre.vida + " ataca a " + ejercitoTrolls.get(randomTroll));
                        hombre.ataca(ejercitoTrolls.get(randomTroll));
                    }
                    System.out.println("_______________________________");
                }
            }
        }
    }

    public void ataqueTrolls() {
        for (Personaje troll : ejercitoTrolls) {
            // ataque ejercito de trolls
            int randomHombre = random.nextInt(ejercitoHombres.size());

            if (ejercitoHombres.get(randomHombre).vida <= 0) {
                System.out.println(troll.nombre + " ataca a " + ejercitoHombres.get(randomHombre) + "\n" + ejercitoHombres.get(randomHombre).nombre + " ha muerto");
                System.out.println("Se ha eliminado del ejercito de hombres a " + (ejercitoHombres.get(randomHombre).nombre));
                ejercitoHombres.remove(ejercitoHombres.get(randomHombre));
                if (ejercitoHombres.size() <= 0) {
                    System.out.println("Ya no quedan miembros en el ejercito de Heroes!!\nLos trolls han conquistado la batalla\nAhora comienza LA ERA DE LOS TROLLS");
                    break;
                } else {
                    System.out.println("_______________________________");
                }

                // mostrar los que quedan en el array a modo de debug
                infoEjercitoHombres();
            } else {
                System.out.println(troll.nombre + ", " + troll.vida + " ataca a " + ejercitoHombres.get(randomHombre));
                troll.ataca(ejercitoHombres.get(randomHombre));
                System.out.println("_______________________________");
            }
        }
    }

    /**
     * permite comenzar la batalla. los turnos iran en funcion de un int random
     * para trolls y heroes, el turno de ataque será para el que obtenga el int
     * mayor en caso de empate no ataca ninguno la batalla continua hasta que no
     * queden mas items en un array.
     */
    public void comenzarBatalla() {

        System.out.println("Que comience al batalla de helm!!!!");
        //turnos
        int tiradaTroll, tiradaHombre;

        for (Personaje troll : ejercitoTrolls) {
            for (Personaje hombre : ejercitoHombres) {
                while (troll.vida > 0 && hombre.vida > 0) {
                    messageInfoRondas();
                    tiradaTroll = random.nextInt(12);
                    tiradaHombre = random.nextInt(12);
                    if (tiradaHombre > tiradaTroll) {
                        // ataque ejercito de hombres
                        System.out.println("puntuacion heroes: " + tiradaHombre + " > puntuacion trolls:" + tiradaTroll + " es Momento del ataque de los heroes");
                        ataqueHeroes();
                    } else if (tiradaTroll > tiradaHombre) {
                        // ataque de trolls
                        System.out.println("puntuacion trolls: " + tiradaTroll + " > puntuacion heroes: " + tiradaHombre + " es Momento del ataque de los trolls");
                        ataqueTrolls();
                    } else {
                        System.out.println("Defensa ferrea!");
                    }

                }
            }
        }

    }
    
    /**
     * aporta info del numero de ronda que va la partida
     * contador se define en la linea 28
     */
    public void messageInfoRondas() {        
        contador++;
        System.out.println("");
        System.out.println("**********");
        System.out.println("Ronda: " + contador);
        System.out.println("**********");
        System.out.println("");
    }
}
