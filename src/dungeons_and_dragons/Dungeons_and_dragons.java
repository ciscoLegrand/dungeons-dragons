package dungeons_and_dragons;

import dungeons_and_dragons.methods.Methods;

import java.util.Scanner;

/**
 *
 * @author cisco
 */
public class Dungeons_and_dragons {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Methods fun = new Methods();
        Scanner s = new Scanner(System.in);
        fun.addAtaqueRandomFromList();
        try {
            System.out.println("introduce una opcion:\n\n1.- Crear ejercitos\n2.- Salir");
            fun.menuprincipal();
        } catch (Exception e) {
            System.out.println("no has introducido una opcion valida\n" + e.getMessage() +" || "+ e.toString() +" || " + e.getLocalizedMessage());
        } finally {
            System.out.println("Aplicacion Cerrada forzosamente!!");
        }
    }
}
