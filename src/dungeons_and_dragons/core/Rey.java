/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dungeons_and_dragons.core;

import Strategy.Ataque;
import Strategy.AtaqueEspada;

/**
 *
 * @author cisco
 */
public class Rey extends Personaje {

    //atributos
    //constructores
    public Rey(String pNombre) {
        this.nombre = pNombre;
        this.vida = 2000;
    }
    
    //getters & setters
    //metodos
    @Override
    public void ataca(Personaje pEnemigo) {
        this.ataque.lanzaAtaque(pEnemigo);
    }

}
