/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dungeons_and_dragons.core;

import Strategy.Ataque;
import Strategy.AtaqueCuchillo;

/**
 *
 * @author cisco
 */
public class Troll extends Personaje {

    //atributos
    //constructores
    public Troll(String pNombre, int pVida) {
        this.nombre = pNombre;
        this.vida = pVida;
    }
    
    //getters & setters
    //metodos
    @Override
    public void ataca(Personaje pEnemigo) {
        this.ataque.lanzaAtaque(pEnemigo);
    }
}
