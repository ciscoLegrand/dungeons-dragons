/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dungeons_and_dragons.core;

import Strategy.Ataque;
import java.util.ArrayList;

/**
 *
 * @author cisco
 */
public abstract class Personaje {

    //atributos
    public static ArrayList<Personaje> ejercitoHombres = new ArrayList<Personaje>();
    public static ArrayList<Personaje> ejercitoTrolls = new ArrayList<Personaje>();

    public String nombre;
    public Ataque ataque;
    public int vida;

    //constructores
    public Personaje() { }

    //getters & setters
    public void setAtaque(Ataque pAtaque) {
        this.ataque = pAtaque;
    }
    
    //metodos
    @Override
    public String toString() {
        return '[' + nombre + ']';
    }

    public abstract void ataca(Personaje pEnemigo);

}
