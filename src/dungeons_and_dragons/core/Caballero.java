/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dungeons_and_dragons.core;

import Strategy.Ataque;
import Strategy.AtaqueEspada;

/**
 *
 * @author cisco
 */
public class Caballero extends Personaje {

    //atributos
    //constructores
    public Caballero(String pNombre) {
        this.nombre = pNombre;
        this.vida = 1500;
    }

    //getters & setters
    //metodos   
    @Override
    public void ataca(Personaje pEnemigo) {
        this.ataque.lanzaAtaque(pEnemigo);
    }

}
